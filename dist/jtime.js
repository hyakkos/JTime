(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/constants.js":
/*!**************************!*\
  !*** ./src/constants.js ***!
  \**************************/
/*! exports provided: YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, MILLISECOND, QUARTER, WEEK, MS_PER_SECOND, MS_PER_MINUTE, MS_PER_HOUR, MS_PER_DAY, MS_PER_WEEK */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"YEAR\", function() { return YEAR; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MONTH\", function() { return MONTH; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DAY\", function() { return DAY; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"HOUR\", function() { return HOUR; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MINUTE\", function() { return MINUTE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SECOND\", function() { return SECOND; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MILLISECOND\", function() { return MILLISECOND; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"QUARTER\", function() { return QUARTER; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"WEEK\", function() { return WEEK; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MS_PER_SECOND\", function() { return MS_PER_SECOND; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MS_PER_MINUTE\", function() { return MS_PER_MINUTE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MS_PER_HOUR\", function() { return MS_PER_HOUR; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MS_PER_DAY\", function() { return MS_PER_DAY; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MS_PER_WEEK\", function() { return MS_PER_WEEK; });\nvar YEAR = 'year';\nvar MONTH = 'month';\nvar DAY = 'day';\nvar HOUR = 'hour';\nvar MINUTE = 'minute';\nvar SECOND = 'second';\nvar MILLISECOND = 'millisecond';\nvar QUARTER = \"quarter\";\nvar WEEK = 'week';\nvar MS_PER_SECOND = 1000;\nvar MS_PER_MINUTE = 60 * MS_PER_SECOND;\nvar MS_PER_HOUR = 60 * MS_PER_MINUTE;\nvar MS_PER_DAY = 24 * MS_PER_HOUR;\nvar MS_PER_WEEK = 7 * MS_PER_DAY;\n\n//# sourceURL=webpack:///./src/constants.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: JTime */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"JTime\", function() { return JTime; });\n/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ \"./src/constants.js\");\n/* harmony import */ var _lib_create__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lib/create */ \"./src/lib/create.js\");\n/* harmony import */ var _lib_format__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lib/format */ \"./src/lib/format.js\");\n/* harmony import */ var _lib_parse__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lib/parse */ \"./src/lib/parse.js\");\n/* harmony import */ var _utils_calcTime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils/calcTime */ \"./src/utils/calcTime.js\");\n/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils/common */ \"./src/utils/common.js\");\n/* harmony import */ var _utils_compare__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils/compare */ \"./src/utils/compare.js\");\nfunction _typeof(o) { \"@babel/helpers - typeof\"; return _typeof = \"function\" == typeof Symbol && \"symbol\" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && \"function\" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? \"symbol\" : typeof o; }, _typeof(o); }\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, \"prototype\", { writable: false }); return Constructor; }\nfunction _toPropertyKey(arg) { var key = _toPrimitive(arg, \"string\"); return _typeof(key) === \"symbol\" ? key : String(key); }\nfunction _toPrimitive(input, hint) { if (_typeof(input) !== \"object\" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || \"default\"); if (_typeof(res) !== \"object\") return res; throw new TypeError(\"@@toPrimitive must return a primitive value.\"); } return (hint === \"string\" ? String : Number)(input); }\n\n\n\n\n\n\n\nvar JTime = /*#__PURE__*/function () {\n  function JTime() {\n    _classCallCheck(this, JTime);\n    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {\n      args[_key] = arguments[_key];\n    }\n    this._date = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(args);\n    this._parse();\n  }\n  _createClass(JTime, [{\n    key: \"format\",\n    value: function format() {\n      var formatStr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'YYYY-MM-DD hh:mm:ss';\n      return Object(_lib_format__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(this._dateObj, formatStr);\n    }\n  }, {\n    key: \"_parse\",\n    value: function _parse() {\n      this._dateObj = Object(_lib_parse__WEBPACK_IMPORTED_MODULE_3__[\"default\"])(this._date);\n    }\n  }, {\n    key: \"diff\",\n    value: function diff(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return Object(_utils_compare__WEBPACK_IMPORTED_MODULE_6__[\"diff\"])(date, this, unit);\n    }\n  }, {\n    key: \"isSame\",\n    value: function isSame(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return this.diff(date, unit) === 0;\n    }\n  }, {\n    key: \"isBeforeOrSame\",\n    value: function isBeforeOrSame(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return this.diff(date, unit) <= 0;\n    }\n  }, {\n    key: \"isBefore\",\n    value: function isBefore(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return this.diff(date, unit) < 0;\n    }\n  }, {\n    key: \"isAfterOrSame\",\n    value: function isAfterOrSame(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return this.diff(date, unit) >= 0;\n    }\n  }, {\n    key: \"isAfter\",\n    value: function isAfter(date) {\n      var unit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return this.diff(date, unit) > 0;\n    }\n  }, {\n    key: \"isBetween\",\n    value: function isBetween(start, end) {\n      var unit = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"];\n      return Object(_utils_compare__WEBPACK_IMPORTED_MODULE_6__[\"between\"])(start, end, this, unit);\n    }\n  }, {\n    key: \"isToday\",\n    value: function isToday() {\n      return this.isBetween(this.clone().startOf(_constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]), this.clone.endOf(_constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]), _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]);\n    }\n  }, {\n    key: \"add\",\n    value: function add(number, unit) {\n      if (Object(_utils_common__WEBPACK_IMPORTED_MODULE_5__[\"isNumber\"])(number)) {\n        if ([_constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]].includes(unit)) {\n          var timeDistance = Object(_utils_calcTime__WEBPACK_IMPORTED_MODULE_4__[\"default\"])(number, unit);\n          this._date.setMilliseconds(this._dateObj.millisecond + timeDistance);\n          this._parse();\n        } else {\n          switch (unit) {\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"YEAR\"]:\n              this._date.setFullYear(this._dateObj.year + number);\n              this._parse();\n              break;\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"QUARTER\"]:\n              this._date.setMonth(this._dateObj.month + 3 * number - 1);\n              this._parse();\n              break;\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MONTH\"]:\n              this._date.setMonth(this._dateObj.month + number - 1);\n              this._parse();\n              break;\n          }\n        }\n      }\n      return this;\n    }\n  }, {\n    key: \"subtract\",\n    value: function subtract(number, unit) {\n      if (Object(_utils_common__WEBPACK_IMPORTED_MODULE_5__[\"isNumber\"])(number)) {\n        if ([_constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"], _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]].includes(unit)) {\n          var timeDistance = Object(_utils_calcTime__WEBPACK_IMPORTED_MODULE_4__[\"default\"])(number, unit);\n          this._date.setMilliseconds(this._dateObj.millisecond - timeDistance);\n          this._parse();\n        } else {\n          switch (unit) {\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"YEAR\"]:\n              this._date.setFullYear(this._dateObj.year - number);\n              this._parse();\n              break;\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"QUARTER\"]:\n              this._date.setMonth(this._dateObj.month - 3 * number - 1);\n              this._parse();\n              break;\n            case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MONTH\"]:\n              this._date.setMonth(this._dateObj.month - number - 1);\n              this._parse();\n              break;\n          }\n        }\n      }\n      return this;\n    }\n  }, {\n    key: \"startOf\",\n    value: function startOf(unit) {\n      var time;\n      switch (unit) {\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"YEAR\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, 1, 1).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"QUARTER\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, Math.floor(this._dateObj.month / 3) * 3, 1).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MONTH\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day - this._dateObj.week).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute).valueOf();\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute, this._dateObj.second).valueOf();\n          break;\n      }\n      if (time) {\n        this._date.setTime(time);\n        this._parse();\n      }\n      return this;\n    }\n  }, {\n    key: \"endOf\",\n    value: function endOf(unit) {\n      var time;\n      switch (unit) {\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"YEAR\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year + 1, 1, 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"QUARTER\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, Math.ceil(this._dateObj.month / 3) * 3 + 1, 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MONTH\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month + 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day - this._dateObj.week + 7 + 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day + 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour + 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute + 1).valueOf() - 1;\n          break;\n        case _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"]:\n          time = Object(_lib_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute, this._dateObj.second + 1).valueOf() - 1;\n          break;\n      }\n      if (time) {\n        this._date.setTime(time);\n        this._parse();\n      }\n      return this;\n    }\n  }, {\n    key: \"clone\",\n    value: function clone() {\n      return new JTime(this._dateObj.millisecond);\n    }\n  }, {\n    key: \"valueOf\",\n    value: function valueOf() {\n      return this._date.valueOf();\n    }\n  }]);\n  return JTime;\n}();\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/lib/create.js":
/*!***************************!*\
  !*** ./src/lib/create.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return create; });\n/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/common */ \"./src/utils/common.js\");\n\nfunction createWithTimeStamp(timestamp) {\n  return new Date(timestamp);\n}\nfunction createWithNow() {\n  return new Date();\n}\nfunction createWithFragment(year, month, day, hours, minutes, seconds, milliseconds) {\n  return new Date(year, month - 1, day, hours, minutes, seconds, milliseconds);\n}\nfunction createWithString(dateString) {\n  return new Date(dateString);\n}\nfunction create() {\n  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {\n    args[_key] = arguments[_key];\n  }\n  if (args.length === 0) {\n    return createWithNow();\n  } else if (args.length === 1) {\n    if (Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"isNumber\"])(args[0])) {\n      return createWithTimeStamp(args[0]);\n    } else if (Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"isString\"])(args[0])) {\n      return createWithString(args[0]);\n    } else if (Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"isDate\"])(args[0])) {\n      return args[0];\n    } else {\n      return createWithNow();\n    }\n  } else {\n    var _args$ = args[0],\n      year = _args$ === void 0 ? 0 : _args$,\n      _args$2 = args[1],\n      month = _args$2 === void 0 ? 1 : _args$2,\n      _args$3 = args[2],\n      day = _args$3 === void 0 ? 1 : _args$3,\n      _args$4 = args[3],\n      hours = _args$4 === void 0 ? 0 : _args$4,\n      _args$5 = args[4],\n      minutes = _args$5 === void 0 ? 0 : _args$5,\n      _args$6 = args[5],\n      seconds = _args$6 === void 0 ? 0 : _args$6,\n      _args$7 = args[6],\n      milliseconds = _args$7 === void 0 ? 0 : _args$7;\n    return createWithFragment(year, month, day, hours, minutes, seconds, milliseconds);\n  }\n}\n\n//# sourceURL=webpack:///./src/lib/create.js?");

/***/ }),

/***/ "./src/lib/format.js":
/*!***************************!*\
  !*** ./src/lib/format.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return format; });\n/* harmony import */ var _utils_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/common */ \"./src/utils/common.js\");\nvar _this = undefined;\nfunction _newArrowCheck(innerThis, boundThis) { if (innerThis !== boundThis) { throw new TypeError(\"Cannot instantiate an arrow function\"); } }\n\nvar formatMethods = {\n  'Y+': function Y(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return dateObj.year.toString().slice(-match.length);\n  }.bind(undefined),\n  'M+': function M(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"zeroFill\"])(match.length, dateObj.month + \"\");\n  }.bind(undefined),\n  'D+': function D(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"zeroFill\"])(match.length, dateObj.day + \"\");\n  }.bind(undefined),\n  \"h+\": function h(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"zeroFill\"])(match.length, dateObj.hour + \"\");\n  }.bind(undefined),\n  \"m+\": function m(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"zeroFill\"])(match.length, dateObj.minute + \"\");\n  }.bind(undefined),\n  \"s+\": function s(dateObj, match) {\n    _newArrowCheck(this, _this);\n    return Object(_utils_common__WEBPACK_IMPORTED_MODULE_0__[\"zeroFill\"])(match.length, dateObj.second + \"\");\n  }.bind(undefined),\n  'S': function S(dateObj) {\n    _newArrowCheck(this, _this);\n    return dateObj.millisecond + \"\";\n  }.bind(undefined)\n};\nfunction format(dateObj, formatStr) {\n  for (var key in formatMethods) {\n    var tempReg = new RegExp(\"(\".concat(key, \")\"));\n    var match = tempReg.exec(formatStr);\n    if (match) {\n      formatStr = formatStr.replace(match[1], formatMethods[key](dateObj, match[1]));\n    }\n  }\n  return formatStr;\n}\n\n//# sourceURL=webpack:///./src/lib/format.js?");

/***/ }),

/***/ "./src/lib/parse.js":
/*!**************************!*\
  !*** ./src/lib/parse.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return parse; });\n/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants */ \"./src/constants.js\");\n/* harmony import */ var _create__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create */ \"./src/lib/create.js\");\n\n\nfunction getFullYears(date) {\n  return date.getFullYear();\n}\nfunction getMonth(date) {\n  return date.getMonth() + 1;\n}\nfunction getDate(date) {\n  return date.getDate();\n}\nfunction getHours(date) {\n  return date.getHours();\n}\nfunction getMinutes(date) {\n  return date.getMinutes();\n}\nfunction getSeconds(date) {\n  return date.getSeconds();\n}\nfunction getMilliseconds(date) {\n  return date.getMilliseconds();\n}\nfunction getDay(date) {\n  return date.getDay();\n}\nfunction getDays(diffMillisecond) {\n  return Math.floor(diffMillisecond / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_DAY\"]);\n}\nfunction getWeeks(diffMillisecond) {\n  return Math.floor(diffMillisecond / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_WEEK\"]);\n}\nfunction getQuarter(month) {\n  if (month < 4) {\n    return 1;\n  } else if (month < 7) {\n    return 2;\n  } else if (month < 10) {\n    return 3;\n  } else {\n    return 4;\n  }\n}\nfunction parse(date) {\n  var result = {\n    year: getFullYears(date),\n    month: getMonth(date),\n    day: getDate(date),\n    hour: getHours(date),\n    minute: getMinutes(date),\n    second: getSeconds(date),\n    millisecond: getMilliseconds(date),\n    week: getDay(date)\n  };\n  var diffMillisecond = date.valueOf() - Object(_create__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(result.year, 1, 1).valueOf();\n  result.days = getDays(diffMillisecond);\n  result.weeks = getWeeks(diffMillisecond);\n  result.quarter = getQuarter(result.month);\n  return result;\n}\n\n//# sourceURL=webpack:///./src/lib/parse.js?");

/***/ }),

/***/ "./src/utils/calcTime.js":
/*!*******************************!*\
  !*** ./src/utils/calcTime.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return calcTime; });\n/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants */ \"./src/constants.js\");\n/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./common */ \"./src/utils/common.js\");\n\n\nfunction calcTime(number, unit) {\n  if (Object(_common__WEBPACK_IMPORTED_MODULE_1__[\"isNumber\"])(number)) {\n    switch (unit) {\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"]:\n        return number;\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"]:\n        return number * _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_SECOND\"];\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"]:\n        return number * _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_MINUTE\"];\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"]:\n        return number * _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_HOUR\"];\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"DAY\"]:\n        return number * _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_DAY\"];\n      case _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]:\n        return number * _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_WEEK\"];\n    }\n  }\n}\n\n//# sourceURL=webpack:///./src/utils/calcTime.js?");

/***/ }),

/***/ "./src/utils/common.js":
/*!*****************************!*\
  !*** ./src/utils/common.js ***!
  \*****************************/
/*! exports provided: default, isArray, isDate, isFunction, isNumber, isObjectEmpty, isObject, isString, isUndefined, isJTime, isLeapYear, zeroFill */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return hasOwnProp; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isArray\", function() { return isArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isDate\", function() { return isDate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isFunction\", function() { return isFunction; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isNumber\", function() { return isNumber; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isObjectEmpty\", function() { return isObjectEmpty; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isObject\", function() { return isObject; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isString\", function() { return isString; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isUndefined\", function() { return isUndefined; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isJTime\", function() { return isJTime; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isLeapYear\", function() { return isLeapYear; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"zeroFill\", function() { return zeroFill; });\n/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! .. */ \"./src/index.js\");\n\nfunction hasOwnProp(a, b) {\n  return Object.prototype.hasOwnProperty.call(a, b);\n}\nfunction isArray(input) {\n  return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';\n}\nfunction isDate(input) {\n  return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';\n}\nfunction isFunction(input) {\n  return typeof Function !== 'undefined' && input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';\n}\nfunction isNumber(input) {\n  return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';\n}\nfunction isObjectEmpty(obj) {\n  if (Object.getOwnPropertyNames) {\n    return Object.getOwnPropertyNames(obj).length === 0;\n  } else {\n    var k;\n    for (k in obj) {\n      if (hasOwnProp(obj, k)) {\n        return false;\n      }\n    }\n    return true;\n  }\n}\nfunction isObject(input) {\n  return input != null && Object.prototype.toString.call(input) === '[object Object]';\n}\nfunction isString(input) {\n  return typeof input === 'string' || input instanceof String;\n}\nfunction isUndefined(input) {\n  return input === void 0;\n}\nfunction isJTime(input) {\n  return input instanceof ___WEBPACK_IMPORTED_MODULE_0__[\"JTime\"];\n}\nfunction isLeapYear(year) {\n  return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;\n}\nfunction zeroFill(lenth, input) {\n  if (input.length >= lenth) {\n    return (+input).toString();\n  } else {\n    var zeroLength = lenth - input.length;\n    var result = '';\n    for (var i = 0; i < zeroLength; i++) {\n      result += '0';\n    }\n    return result += input;\n  }\n}\n\n//# sourceURL=webpack:///./src/utils/common.js?");

/***/ }),

/***/ "./src/utils/compare.js":
/*!******************************!*\
  !*** ./src/utils/compare.js ***!
  \******************************/
/*! exports provided: monthDiff, diff, between */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"monthDiff\", function() { return monthDiff; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"diff\", function() { return diff; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"between\", function() { return between; });\n/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants */ \"./src/constants.js\");\n/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../index */ \"./src/index.js\");\n/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./common */ \"./src/utils/common.js\");\n\n\n\nfunction monthDiff(date1, date2) {\n  var diffDate = date1;\n  if (!Object(_common__WEBPACK_IMPORTED_MODULE_2__[\"isJTime\"])(diffDate)) {\n    diffDate = new _index__WEBPACK_IMPORTED_MODULE_1__[\"JTime\"](date1);\n  }\n  var year = diffDate._dateObj.year - date2._dateObj.year;\n  var month = diffDate._dateObj.month - date2._dateObj.month;\n  return year * 12 + month;\n}\nfunction diff(date1, date2, unit) {\n  var diffDate = date1;\n  if (!Object(_common__WEBPACK_IMPORTED_MODULE_2__[\"isJTime\"])(diffDate)) {\n    diffDate = new _index__WEBPACK_IMPORTED_MODULE_1__[\"JTime\"](date1);\n  }\n  switch (unit) {\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MILLISECOND\"]:\n      return diffDate.valueOf() - date2.valueOf();\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"SECOND\"]:\n      return Math.floor((diffDate.valueOf() - date2.valueOf()) / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_SECOND\"]);\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MINUTE\"]:\n      return Math.floor((diffDate.valueOf() - date2.valueOf()) / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_MINUTE\"]);\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"HOUR\"]:\n      return Math.floor((diffDate.valueOf() - date2.valueOf()) / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_HOUR\"]);\n    case DAY:\n      return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_DAY);\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"WEEK\"]:\n      return Math.floor((diffDate.valueOf() - date2.valueOf()) / _constants__WEBPACK_IMPORTED_MODULE_0__[\"MS_PER_WEEK\"]);\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"MONTH\"]:\n      return monthDiff(diffDate, data2);\n    case _constants__WEBPACK_IMPORTED_MODULE_0__[\"YEAR\"]:\n      return Math.floor(monthDiff(diffDate, data2) / 12);\n  }\n}\nfunction between(start, end, date, unit) {\n  var startDate = start;\n  if (!Object(_common__WEBPACK_IMPORTED_MODULE_2__[\"isJTime\"])(startDate)) {\n    startDate = new _index__WEBPACK_IMPORTED_MODULE_1__[\"JTime\"](start);\n  }\n  startDate.startOf(unit);\n  var endDate = end;\n  if (!Object(_common__WEBPACK_IMPORTED_MODULE_2__[\"isJTime\"])(endDate)) {\n    endDate = new _index__WEBPACK_IMPORTED_MODULE_1__[\"JTime\"](end);\n  }\n  startDate.endOf(unit);\n  var startTime = start.valueOf();\n  var endTime = end.valueOf();\n  var dateTime = date.valueOf();\n  return dateTime <= endTime && dateTime >= startTime;\n}\n\n//# sourceURL=webpack:///./src/utils/compare.js?");

/***/ })

/******/ });
});