import { HOUR, MILLISECOND, MINUTE, MONTH, MS_PER_HOUR, MS_PER_MINUTE, MS_PER_SECOND, MS_PER_WEEK, QUARTER, SECOND, WEEK, YEAR } from "../constants";
import { JTime } from "../index";
import { isJTime } from "./common";

export function monthDiff(date1, date2) {
    let diffDate = date1;
    if (!isJTime(diffDate)) {
        diffDate = new JTime(date1)
    }
    const year = diffDate._dateObj.year - date2._dateObj.year;
    const month = diffDate._dateObj.month - date2._dateObj.month;
    return year * 12 + month;
}

export function diff(date1, date2, unit) {
    let diffDate = date1;
    if (!isJTime(diffDate)) {
        diffDate = new JTime(date1)
    }
    switch (unit) {
        case MILLISECOND:
            return diffDate.valueOf() - date2.valueOf()
        case SECOND:
            return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_SECOND)
        case MINUTE:
            return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_MINUTE)
        case HOUR:
            return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_HOUR)
        case DAY:
            return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_DAY)
        case WEEK:
            return Math.floor((diffDate.valueOf() - date2.valueOf()) / MS_PER_WEEK)
        case MONTH:
            return monthDiff(diffDate, data2);
        case YEAR:
            return Math.floor(monthDiff(diffDate, data2) / 12)
    }
}

export function between(start, end, date, unit) {
    let startDate = start;
    if (!isJTime(startDate)) {
        startDate = new JTime(start)
    }
    startDate.startOf(unit);
    let endDate = end;
    if (!isJTime(endDate)) {
        endDate = new JTime(end)
    }
    startDate.endOf(unit);
    const startTime = start.valueOf();
    const endTime = end.valueOf();
    const dateTime = date.valueOf();
    return dateTime <= endTime && dateTime >= startTime;
}