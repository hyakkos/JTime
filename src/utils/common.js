import { JTime } from "..";

export default function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
}


export function isArray(input) {
    return (
        input instanceof Array ||
        Object.prototype.toString.call(input) === '[object Array]'
    );
}

export function isDate(input) {
    return (
        input instanceof Date ||
        Object.prototype.toString.call(input) === '[object Date]'
    );
}

export function isFunction(input) {
    return (
        (typeof Function !== 'undefined' && input instanceof Function) ||
        Object.prototype.toString.call(input) === '[object Function]'
    );
}

export function isNumber(input) {
    return (
        typeof input === 'number' ||
        Object.prototype.toString.call(input) === '[object Number]'
    );
}

export function isObjectEmpty(obj) {
    if (Object.getOwnPropertyNames) {
        return Object.getOwnPropertyNames(obj).length === 0;
    } else {
        var k;
        for (k in obj) {
            if (hasOwnProp(obj, k)) {
                return false;
            }
        }
        return true;
    }
}

export function isObject(input) {
    return (
        input != null &&
        Object.prototype.toString.call(input) === '[object Object]'
    );
}


export function isString(input) {
    return typeof input === 'string' || input instanceof String;
}

export function isUndefined(input) {
    return input === void 0;
}

export function isJTime(input) {
    return input instanceof JTime;
}


export function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}


export function zeroFill(lenth, input) {
    if (input.length >= lenth) {
        return (+input).toString()
    } else {
        const zeroLength = lenth - input.length;
        let result = '';
        for (let i = 0; i < zeroLength; i++) {
            result += '0'
        }
        return result += input;
    }

}