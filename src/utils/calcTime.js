import { DAY, HOUR, MILLISECOND, MINUTE, MS_PER_DAY, MS_PER_HOUR, MS_PER_MINUTE, MS_PER_SECOND, MS_PER_WEEK, SECOND, WEEK } from "../constants";
import { isNumber } from "./common";

export default function calcTime(number, unit){
    if(isNumber(number)){
        switch(unit){
            case MILLISECOND:
                return number
            case SECOND:
                return number * MS_PER_SECOND
            case MINUTE:
                return number * MS_PER_MINUTE
            case HOUR:
                return number * MS_PER_HOUR
            case DAY:
                return number * MS_PER_DAY;
            case WEEK:
                return number * MS_PER_WEEK
        }
    }
}