import { DAY, HOUR, MILLISECOND, MINUTE, MONTH, QUARTER, SECOND, WEEK, YEAR } from "./constants";
import create from "./lib/create"
import format from "./lib/format"
import parse from "./lib/parse";
import calcTime from "./utils/calcTime"
import { isNumber } from "./utils/common";
import { between, diff } from "./utils/compare";
export class JTime {
    constructor(...args) {
        this._date = create(args);
        this._parse();
    }

    format(formatStr = 'YYYY-MM-DD hh:mm:ss') {
        return format(this._dateObj, formatStr);
    }

    _parse() {
        this._dateObj = parse(this._date)
    }

    diff(date, unit = MILLISECOND) {
        return diff(date, this, unit);
    }

    isSame(date, unit = MILLISECOND) {
        return this.diff(date, unit) === 0
    }
    isBeforeOrSame(date, unit = MILLISECOND) {
        return this.diff(date, unit) <= 0
    }

    isBefore(date, unit = MILLISECOND) {
        return this.diff(date, unit) < 0
    }

    isAfterOrSame(date, unit = MILLISECOND) {
        return this.diff(date, unit) >= 0
    }

    isAfter(date, unit = MILLISECOND) {
        return this.diff(date, unit) > 0
    }

    isBetween(start, end, unit = MILLISECOND) {
        return between(start, end, this, unit);
    }

    isToday() {
        return this.isBetween(this.clone().startOf(DAY), this.clone.endOf(DAY), DAY)
    }

    add(number, unit) {
        if (isNumber(number)) {
            if ([MILLISECOND, SECOND, MINUTE, HOUR, DAY, WEEK].includes(unit)) {
                const timeDistance = calcTime(number, unit);
                this._date.setMilliseconds(this._dateObj.millisecond + timeDistance);
                this._parse();
            } else {
                switch (unit) {
                    case YEAR:
                        this._date.setFullYear(this._dateObj.year + number);
                        this._parse();
                        break;
                    case QUARTER:
                        this._date.setMonth(this._dateObj.month + 3 * number - 1);
                        this._parse();
                        break;
                    case MONTH:
                        this._date.setMonth(this._dateObj.month + number - 1);
                        this._parse();
                        break;
                }
            }
        }
        return this;
    }


    subtract(number, unit) {
        if (isNumber(number)) {
            if ([MILLISECOND, SECOND, MINUTE, HOUR, DAY, WEEK].includes(unit)) {
                const timeDistance = calcTime(number, unit);
                this._date.setMilliseconds(this._dateObj.millisecond - timeDistance);
                this._parse();
            } else {
                switch (unit) {
                    case YEAR:
                        this._date.setFullYear(this._dateObj.year - number);
                        this._parse();
                        break;
                    case QUARTER:
                        this._date.setMonth(this._dateObj.month - 3 * number - 1);
                        this._parse();
                        break;
                    case MONTH:
                        this._date.setMonth(this._dateObj.month - number - 1);
                        this._parse();
                        break;
                }
            }
        }
        return this;
    }

    startOf(unit) {
        let time;
        switch (unit) {
            case YEAR:
                time = create(this._dateObj.year, 1, 1).valueOf();
                break;
            case QUARTER:
                time = create(this._dateObj.year, Math.floor(this._dateObj.month / 3) * 3, 1).valueOf();
                break;
            case MONTH:
                time = create(this._dateObj.year, this._dateObj.month).valueOf();
                break;
            case WEEK:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day - this._dateObj.week).valueOf();
                break;
            case DAY:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day).valueOf();
                break;
            case HOUR:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour).valueOf();
                break;
            case MINUTE:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute).valueOf();
                break;
            case SECOND:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute, this._dateObj.second).valueOf();
                break;
        }
        if (time) {
            this._date.setTime(time);
            this._parse();
        }
        return this;
    }

    endOf(unit) {
        let time;
        switch (unit) {
            case YEAR:
                time = create(this._dateObj.year + 1, 1, 1).valueOf() - 1;
                break;
            case QUARTER:
                time = create(this._dateObj.year, Math.ceil(this._dateObj.month / 3) * 3 + 1, 1).valueOf() - 1;
                break;
            case MONTH:
                time = create(this._dateObj.year, this._dateObj.month + 1).valueOf() - 1;
                break;
            case WEEK:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day - this._dateObj.week + 7 + 1).valueOf() - 1;
                break;
            case DAY:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day + 1).valueOf() - 1;
                break;
            case HOUR:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour + 1).valueOf() - 1;
                break;
            case MINUTE:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute + 1).valueOf() - 1;
                break;
            case SECOND:
                time = create(this._dateObj.year, this._dateObj.month, this._dateObj.day, this._dateObj.hour, this._dateObj.minute, this._dateObj.second + 1).valueOf() - 1;
                break;
        }
        if (time) {
            this._date.setTime(time);
            this._parse();
        }
        return this;
    }

    clone() {
        return new JTime(this._dateObj.millisecond);
    }

    valueOf() {
        return this._date.valueOf()
    }
}