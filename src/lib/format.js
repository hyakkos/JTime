import { zeroFill } from "../utils/common";

const formatMethods = {
    'Y+': (dateObj, match) => dateObj.year.toString().slice(-match.length),
    'M+': (dateObj, match) => zeroFill(match.length, dateObj.month + ""),
    'D+': (dateObj, match) => zeroFill(match.length, dateObj.day + ""),
    "h+": (dateObj, match) => zeroFill(match.length, dateObj.hour + ""),
    "m+": (dateObj, match) => zeroFill(match.length, dateObj.minute + ""),
    "s+": (dateObj, match) => zeroFill(match.length, dateObj.second + ""),
    'S': dateObj => dateObj.millisecond + "",
};

export default function format(dateObj, formatStr) {
    for (const key in formatMethods) {
        const tempReg = new RegExp(`(${key})`);
        const match = tempReg.exec(formatStr);
        if (match) {
            formatStr = formatStr.replace(match[1], formatMethods[key](dateObj, match[1]))
        }
    }
    return formatStr;
}