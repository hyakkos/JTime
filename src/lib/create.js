import { isDate, isNumber, isString } from "../utils/common";

function createWithTimeStamp(timestamp) {
    return new Date(timestamp)
}

function createWithNow() {
    return new Date();
}

function createWithFragment(year, month, day, hours, minutes, seconds, milliseconds) {
    return new Date(year, month - 1, day, hours, minutes, seconds, milliseconds)
}

function createWithString(dateString) {
    return new Date(dateString);
}

export default function create(...args) {
    if (args.length === 0) {
        return createWithNow()
    } else if (args.length === 1) {
        if (isNumber(args[0])) {
            return createWithTimeStamp(args[0])
        } else if (isString(args[0])) {
            return createWithString(args[0])
        }else if (isDate(args[0])) {
            return args[0];
        }  else {
            return createWithNow()
        }
    } else {
        const [year = 0, month = 1, day = 1, hours = 0, minutes = 0, seconds = 0, milliseconds = 0] = args
        return createWithFragment(year, month, day, hours, minutes, seconds, milliseconds)
    }
}