import { MS_PER_DAY, MS_PER_WEEK } from "../constants";
import create from "./create";

function getFullYears(date) {
    return date.getFullYear()
}

function getMonth(date) {
    return date.getMonth() + 1;
}

function getDate(date) {
    return date.getDate()
}

function getHours(date) {
    return date.getHours()
}

function getMinutes(date) {
    return date.getMinutes()
}

function getSeconds(date) {
    return date.getSeconds()
}

function getMilliseconds(date) {
    return date.getMilliseconds()
}

function getDay(date) {
    return date.getDay()
}

function getDays(diffMillisecond) {
    return Math.floor(diffMillisecond / MS_PER_DAY)
}

function getWeeks(diffMillisecond) {
    return Math.floor(diffMillisecond / MS_PER_WEEK)
}

function getQuarter(month) {
    if (month < 4) {
        return 1
    } else if (month < 7) {
        return 2
    } else if (month < 10) {
        return 3
    } else {
        return 4
    }
}
export default function parse(date) {
    const result = {
        year: getFullYears(date),
        month: getMonth(date),
        day: getDate(date),
        hour: getHours(date),
        minute: getMinutes(date),
        second: getSeconds(date),
        millisecond: getMilliseconds(date),
        week: getDay(date)
    }
    const diffMillisecond = date.valueOf() - create(result.year, 1, 1).valueOf();
    result.days = getDays(diffMillisecond);
    result.weeks = getWeeks(diffMillisecond);
    result.quarter = getQuarter(result.month);
    return result;
}